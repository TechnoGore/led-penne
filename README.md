# Led Penne 

Control led strips with both Art-Net and infrared

---

This code allow an aruino to controll a led strip with an infrared remote and with the Art-Net protocol.
It was designed to works with a RGBW led strip but it's easilly adaptable for other leds 

## from IR

with IR, it just allows you to adjust the color of a pre-defined "area" (eg. "from pixel 1 to pixel 50").
1. select one or more areas with the remote
    + use button 1, 2, 3... to select/deselect area
    + or use the "select all", "select none" or "invert selection" buttons
2. adjust the color of selected areas with hue, saturation and brightness controls

Machine is in IR mode by default, on startup.

## From Art-Net

Art-Net is a network protocol to controll lighting equipment (basically DMX over IP). In this mode, you can controll each pixel individually with an Art-Net controller, for example the software [Jinx!](http://www.live-leds.de/).

Modify the setting at the begining of the code to set your SSID and wifi password. The Arduino will get an IP from DHCP.

Also set the number of pixels and the start DMX address and universe.
Each pixel will take 3 DMX channels (RGB), it will be converted to RGBW automatically. If there is the need for more pixels than there is left in the DMX universe, it will continue on the next one with a first channel of the pixel so it isn't cut in half between universe (eg. if pixel number 63 ends at address 510, pixel #64 will start at the address 1 of the next universe and not channel 511 of the first U).

The arduino will switch to Art-Net mode automatically when it starts to recieve Art-Net packets.
It will switch back to IR when it havent recieved Art-Net for 5 minutes.